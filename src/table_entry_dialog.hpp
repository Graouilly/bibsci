// =============================================================================
//  BibSci - Bibliothèque scientifique
// -----------------------------------------------------------------------------
//  src/table_entry_dialog.hpp
// -----------------------------------------------------------------------------
//  Copyright (C) 2024 - Graouilly
//
//  This file is part of BibSci.
//
//  BibSci is free software: you can redistribute it and/or modify it under the
//  terms of the GNU General Public License as published by the Free Software
//  Foundation, version 3 of the License.
//  BibSci is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
//  details.
// 
//  You should have received a copy of the GNU General Public License along with
//  BibSci. If not, see <https://www.gnu.org/licenses/>
// =============================================================================
#ifndef BIBSCI_TABLE_ENTRY_DIALOG_HPP
#define BIBSCI_TABLE_ENTRY_DIALOG_HPP

// Qt Includes
#include <QDialog>    // QDialog
#include <QLineEdit>  // QLineEdit

//! \brief Window for a new table entry.
//! Slots and signals are used so Q_OBJECT macro is mandatory.
//! Consistency of the values is checked before closed the window by calling
//! the slot verify().
class TableEntryDialog : public QDialog
{
   Q_OBJECT

public:
   //! \brief Constructeur.
   //! \param[in] title titre de la fenêtre
   //! \param[in] parent widget père de la fenêtre de dialogue
   TableEntryDialog(const QString &title, QWidget *parent);

   ~TableEntryDialog();

public slots:
   //!@{
   //! \name Slots

   //! \brief Slot de vérification de la cohérence des données.
   //! La fonction appelle les slots accept() si les données sont cohérentes
   //! et reject() si elles ne le sont pas.
   void verify();

   //!@}

public:
   //! \brief Valeur de l'entrée.
   QString Entry() const;

private:
   //! \brief QLineEdit du paramètre
   QLineEdit name_edit_;

};

#endif // BIBSCI_TABLE_ENTRY_DIALOG_HPP
