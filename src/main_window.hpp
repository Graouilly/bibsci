// =============================================================================
//  BibSci - Bibliothèque scientifique
// -----------------------------------------------------------------------------
//  src/main_window.hpp
// -----------------------------------------------------------------------------
//  Copyright (C) 2024 - Graouilly
//
//  This file is part of BibSci.
//
//  BibSci is free software: you can redistribute it and/or modify it under the
//  terms of the GNU General Public License as published by the Free Software
//  Foundation, version 3 of the License.
//  BibSci is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
//  details.
// 
//  You should have received a copy of the GNU General Public License along with
//  BibSci. If not, see <https://www.gnu.org/licenses/>
// =============================================================================
#ifndef BIBSCI_MAIN_WINDOW_HPP
#define BIBSCI_MAIN_WINDOW_HPP

// Qt Includes
#include <QMainWindow>     // QMainWindow
#include <QSqlTableModel>  // QSqlTableModel

//! \brief Main Window
//! Slots and signals are used so Q_OBJECT macro is mandatory.
class MainWindow : public QMainWindow
{
   Q_OBJECT

public slots:
   //!@{
   //! \name Slots

   //! \brief "About" information is requested
   void About();

   //! \brief Types of books shall be managed.
   void ManageTypes();

   //!@}

public:
   //! \brief Constructor.
   explicit MainWindow(QWidget *parent = 0);

   //! \brief Destructor.
   ~MainWindow();

private:
   // The declaration without instantiation triggers a compilation error if
   // there is an implicit call by the compiler.
   MainWindow(const MainWindow&);
   void operator=(const MainWindow&);

   //! \brief SQL table model.
   QSqlTableModel model_;
};

#endif // BIBSCI_MAIN_WINDOW_HPP
