// =============================================================================
//  BibSci - Bibliothèque scientifique
// -----------------------------------------------------------------------------
//  src/table_view_window.hpp
// -----------------------------------------------------------------------------
//  Copyright (C) 2024 - Graouilly
//
//  This file is part of BibSci.
//
//  BibSci is free software: you can redistribute it and/or modify it under the
//  terms of the GNU General Public License as published by the Free Software
//  Foundation, version 3 of the License.
//  BibSci is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
//  details.
// 
//  You should have received a copy of the GNU General Public License along with
//  BibSci. If not, see <https://www.gnu.org/licenses/>
// =============================================================================
#ifndef BIBSCI_TABLE_VIEW_WINDOW_HPP
#define BIBSCI_TABLE_VIEW_WINDOW_HPP

// Inclusions Qt
#include <QWidget>
#include <QSqlTableModel>
#include <QString>
#include <QObject>

//! \brief Fenêtre de visualisation d'une table.
//! L'utilisation des slots et signaux implique l'appel de Q_OBJECT.
//! La cohérence des valeurs est vérifiée avant fermeture de la fenêtre via
//! le slot verify().
class TableViewWindow : public QWidget
{
   Q_OBJECT

public slots:
   //!@{
   //! \name Slots

   //! \brief Ajout d'une entrée.
   void AddEntry();
   //! \brief Suppression d'une entrée.
   void RemoveEntry();
   //! \brief Sauvegarde de la table.
   void Save();

   //!@}

public:
   //! \brief Constructeur
   //! \param[in] model référence du modèle de la table
   //! \param[in] table_name nom de la table SQL
   //! \param[in] parent widget père de la fenêtre
   //! model pourra être modifié.
   TableViewWindow(QSqlTableModel &model, const QString &table_name,
                   QWidget *parent = nullptr);

   //! \brief Destructeur.
   ~TableViewWindow();

private:
   //! \brief Référence du modèle de table SQL.
   QSqlTableModel &model_ref_;
   //! \brief Nom de la table SQL.
   QString table_name_;
};

#endif // BIBSCI_TABLE_VIEW_WINDOW_HPP
