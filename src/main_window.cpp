// =============================================================================
//  BibSci - Bibliothèque scientifique
// -----------------------------------------------------------------------------
//  src/main_window.cpp
// -----------------------------------------------------------------------------
//  Copyright (C) 2024 - Graouilly
//
//  This file is part of BibSci.
//
//  BibSci is free software: you can redistribute it and/or modify it under the
//  terms of the GNU General Public License as published by the Free Software
//  Foundation, version 3 of the License.
//  BibSci is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
//  details.
// 
//  You should have received a copy of the GNU General Public License along with
//  BibSci. If not, see <https://www.gnu.org/licenses/>
// =============================================================================

// Inclusions Qt
#include <QtWidgets>       // QMenuBar, ...
#include <QtSql>           // QtSql
#include <QSqlTableModel>  // QSqlTableModel
#include <QTableView>      // QTableView

// Inclusions internes
#include "main_window.hpp"
#include "table_view_window.hpp"


// =============================================================================
//
//  CONSTRUCTEURS ET DESTRUCTEURS
//
// =============================================================================

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
   // Création du menu
   QMenuBar *menu_bar = new QMenuBar;

   QMenu *file_menu = new QMenu(tr("Li&brary"), this);
   //QAction *openAction = file_menu->addAction(tr("&Open"));
   //connect(openAction, &QAction::triggered, this, &MainWindow::OpenLib);
   //QAction *newAction = file_menu->addAction(tr("&New"));
   //connect(newAction, &QAction::triggered, this, &MainWindow::NewLib);
   QAction *exitAction = file_menu->addAction(tr("E&xit"));
   connect(exitAction, SIGNAL(triggered()), this, SLOT(close()));

   QMenu *help_menu = new QMenu(tr("&Help"), this);
   QAction *about_action = new QAction(tr("&About..."), this);
   about_action->setStatusTip(tr("Application information"));
   connect(about_action, &QAction::triggered, this, &MainWindow::About);
   help_menu->addAction(about_action);
   QAction *aboutqt_action = new QAction(tr("About &Qt..."), this);
   aboutqt_action->setStatusTip(tr("Qt-related information"));
   connect(aboutqt_action, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
   help_menu->addAction(aboutqt_action);

   menu_bar->addMenu(file_menu);
   menu_bar->addMenu(help_menu);

   QPushButton *btn_types = new QPushButton(tr("Types..."));
   connect(btn_types, SIGNAL(clicked(bool)), this, SLOT(ManageTypes()));

   // Layout global et widget central
   QVBoxLayout *mainLayout = new QVBoxLayout;
   mainLayout->addWidget(menu_bar);
   mainLayout->addWidget(btn_types);

   QWidget *widget = new QWidget();
   widget->setLayout(mainLayout);
   setCentralWidget(widget);

   // Ouverture de la bibliothèque par défaut
   QSqlDatabase db = QSqlDatabase::database();
   QString dbn = QCoreApplication::applicationDirPath() + "\\biblio2.bib";
   db.setDatabaseName(dbn);
   if (!db.open()) {
       QMessageBox::critical(0, qApp->tr("Database cannot be opened"),
           qApp->tr("Impossible to connect with library \n\n") + dbn,
                    QMessageBox::Cancel);
       return;
   }

   setWindowTitle(IHM_PRODUCT);
}

MainWindow::~MainWindow()
{

}

// =============================================================================
//
//  SLOTS ET SIGNAUX
//
// =============================================================================

void MainWindow::ManageTypes()
{
   TableViewWindow *tvw = new TableViewWindow(model_, "TYPES_OUVRAGES");
   tvw->show();
}


void MainWindow::About()
{
   QMessageBox::about(this, tr("About %1").arg(IHM_PRODUCT),
                      tr("<h3>%1 v%2</h3>"
                         "<I>%3</I>"
                         "<p>Build date : %4 %5<p/>"
                         "<p>Git        : %6<p/>"
                         "<p>(c) 2021 - %7, Damien GILLE</p>"
                         ).arg(IHM_PRODUCT, IHM_VERSION, IHM_DESCRIPTION,
                               __DATE__, __TIME__, GIT_VERSION, IHM_COPYRIGHT));
}


//void MainWindow::NewLib()
//{
//   QString fileName = QFileDialog::getSaveFileName(this,
//          tr("New library"), "",
//          tr("Library (*.bib);;All files (*)"));
//   if (fileName.isEmpty())
//      return;
//}


//void MainWindow::OpenLib()
//{
//   QString fileName = QFileDialog::getOpenFileName(this,
//          tr("Open a library"), "",
//          tr("Library (*.bib);;All files (*)"));
//   if (fileName.isEmpty())
//      return;
//}


