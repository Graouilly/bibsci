// =============================================================================
//  BibSci - Bibliothèque scientifique
// -----------------------------------------------------------------------------
//  src/table_entry_dialog.cpp
// -----------------------------------------------------------------------------
//  Copyright (C) 2024 - Graouilly
//
//  This file is part of BibSci.
//
//  BibSci is free software: you can redistribute it and/or modify it under the
//  terms of the GNU General Public License as published by the Free Software
//  Foundation, version 3 of the License.
//  BibSci is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
//  details.
// 
//  You should have received a copy of the GNU General Public License along with
//  BibSci. If not, see <https://www.gnu.org/licenses/>
// =============================================================================

// Inclusions Qt
#include <QDialog>           // QDialog
#include <QDialogButtonBox>  // QDialogButtonBox
#include <QGridLayout>       // QGridLayout
#include <QLabel>            // QLabel
#include <QLineEdit>         // QLineEdit
#include <QMessageBox>       // QMessageBox
#include <QString>           // QString

// Header
#include "table_entry_dialog.hpp"


// =============================================================================
//
//  CONSTRUCTEURS ET DESTRUCTEURS
//
// =============================================================================

TableEntryDialog::TableEntryDialog(const QString &title, QWidget *parent)
    : QDialog(parent), name_edit_()
{
   QLabel *name_label = new QLabel(tr("Name:"));
   QDialogButtonBox *btns = new QDialogButtonBox(QDialogButtonBox::Ok |
                                                 QDialogButtonBox::Cancel);
   connect(btns, &QDialogButtonBox::accepted, this, &TableEntryDialog::verify);
   connect(btns, &QDialogButtonBox::rejected, this, &TableEntryDialog::reject);

   QGridLayout *main_lo = new QGridLayout;
   main_lo->addWidget(name_label, 0, 0);
   main_lo->addWidget(&name_edit_, 0, 1);
   main_lo->addWidget(btns, 1, 0, 1, 2);
   setLayout(main_lo);

   setWindowTitle(title);
}

TableEntryDialog::~TableEntryDialog()
{

}


// =============================================================================
//
//  SLOTS ET SIGNAUX
//
// =============================================================================

void TableEntryDialog::verify()
{
   if (!name_edit_.text().isEmpty()) {
      accept();
      return;
   }
   QMessageBox::warning(
      this, tr("Incomplete entry"),
      tr("The form does not contain all the required information.\n"
         "It cannot be added to the document types"),
      QMessageBox::Ok);
   reject();
}


// =============================================================================
//
//  FONCTIONS
//
// =============================================================================

QString TableEntryDialog::Entry() const
{
   return name_edit_.text();
}
