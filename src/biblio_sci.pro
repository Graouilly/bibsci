// =============================================================================
//  BibSci - Bibliothèque scientifique
// -----------------------------------------------------------------------------
//  src/biblio_sci.pro
// -----------------------------------------------------------------------------
//  Copyright (C) 2024 - Graouilly
//
//  This file is part of BibSci.
//
//  BibSci is free software: you can redistribute it and/or modify it under the
//  terms of the GNU General Public License as published by the Free Software
//  Foundation, version 3 of the License.
//  BibSci is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
//  details.
// 
//  You should have received a copy of the GNU General Public License along with
//  BibSci. If not, see <https://www.gnu.org/licenses/>
// =============================================================================

# ------------------------------------------------------------------------------
# Input files
# ------------------------------------------------------------------------------
HEADERS = \
    main_window.hpp \
    table_entry_dialog.hpp \
    table_view_window.hpp
SOURCES = main.cpp \
    main_window.cpp \
    table_entry_dialog.cpp \
    table_view_window.cpp

# ------------------------------------------------------------------------------
# Qt configurations
# ------------------------------------------------------------------------------
QT += sql widgets
requires(qtConfig(tableview))

# install
#target.path = $$[QT_INSTALL_EXAMPLES]/sql/tablemodel
#INSTALLS += target

# ------------------------------------------------------------------------------
# Target properties
# ------------------------------------------------------------------------------
TARGET = BibSci
TEMPLATE = app
CONFIG(debug, debug|release) {
   # For debug configuration, _d is appended to the binary name.
   TARGET = $$join(TARGET,,,_d)
}

#TRANSLATIONS = t1_fr.ts qt_fr.ts

# Versioning
VERSION = 0.0.0
GIT_VERSION = $$system(git show-ref --hash)

# Properties of the binary under Windows
QMAKE_TARGET_COMPANY = "-"
QMAKE_TARGET_PRODUCT = "BibSci"
QMAKE_TARGET_DESCRIPTION = "Scientific library"
BUILD_YEAR = $$str_member($${_DATE_}, -4, -1)
QMAKE_TARGET_COPYRIGHT = "2023-$${BUILD_YEAR} - D. GILLE"

# ------------------------------------------------------------------------------
# Compilation options
# ------------------------------------------------------------------------------
CONFIG += c++14 warn_on

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version.
# disables all the APIs deprecated before Qt 6.0.0
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000

# Defines expected by the application
DEFINES += GIT_VERSION=\\\"$$GIT_VERSION\\\"
DEFINES += IHM_VERSION=\\\"$$VERSION\\\"
DEFINES += IHM_COMPANY=\\\"$$QMAKE_TARGET_COMPANY\\\"
DEFINES += IHM_PRODUCT=\\\"$$QMAKE_TARGET_PRODUCT\\\"
DEFINES += IHM_DESCRIPTION=\"\\\"$$QMAKE_TARGET_DESCRIPTION\"\\\"
DEFINES += IHM_COPYRIGHT=\"\\\"$$QMAKE_TARGET_COPYRIGHT\"\\\"

# ------------------------------------------------------------------------------
# Directories
# ------------------------------------------------------------------------------
# base directory from which everything will be referred relatively.
BASE_DIR = $$PWD/..
# build files' directory gathering all configurations (release, debug, ...).
BUILD_DIR = $$BASE_DIR/build
# binaries' directory gathering all configurations (release, debug, ...).
BIN_DIR = $$BASE_DIR/bin

# BUILD_CFG_SUBDIR is the directory where the configuration is built.
# DESTDIR is the directory where the binaries are stored (variable name expected
# by QMake.
CONFIG(debug, debug|release) {
   # Debug
   BUILD_CFG_SUBDIR = $$BUILD_DIR/debug
   DESTDIR = $$BIN_DIR/debug
}
else {
   # Release
   BUILD_CFG_SUBDIR = $$BUILD_DIR/release
   DESTDIR = $$BIN_DIR/release
}

# objects' directory
OBJECTS_DIR = $$BUILD_CFG_SUBDIR/obj
# user interface files' directory
UI_DIR = $$BUILD_CFG_SUBDIR/ui
# meta object compiler files' directory
MOC_DIR = $$BUILD_CFG_SUBDIR/moc
# resource compiler files' directory
RCC_DIR = $$BUILD_CFG_SUBDIR/rcc
