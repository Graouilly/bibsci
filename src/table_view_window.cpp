// =============================================================================
//  BibSci - Bibliothèque scientifique
// -----------------------------------------------------------------------------
//  src/table_view_window.cpp
// -----------------------------------------------------------------------------
//  Copyright (C) 2024 - Graouilly
//
//  This file is part of BibSci.
//
//  BibSci is free software: you can redistribute it and/or modify it under the
//  terms of the GNU General Public License as published by the Free Software
//  Foundation, version 3 of the License.
//  BibSci is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
//  details.
// 
//  You should have received a copy of the GNU General Public License along with
//  BibSci. If not, see <https://www.gnu.org/licenses/>
// =============================================================================

// Inclusions Qt
#include <QSqlTableModel>  // QSqlTableModel
#include <QString>         // QString
#include <QTableView>      // QTableView
#include <QtSql>           // QtSql
#include <QtWidgets>       // QtWidgets

// Inclusions internes
#include "table_entry_dialog.hpp"  // TableEntryDialog

// Header
#include "table_view_window.hpp"

// =============================================================================
//
//  CONSTRUCTEURS ET DESTRUCTEURS
//
// =============================================================================

TableViewWindow::TableViewWindow(QSqlTableModel &model,
                                 const QString &table_name,
                                 QWidget *parent):
   QWidget(parent), model_ref_(model), table_name_(table_name)
{
   // Contenu de la table SQL
   model_ref_.setTable(table_name_);
   model_ref_.setEditStrategy(QSqlTableModel::OnManualSubmit);
   model_ref_.select();
//   QString s = model_ref_.primaryKey().isEmpty() ? "none" : "'"+model_ref_.primaryKey().cursorName()+"'" ;
//   QMessageBox msg(QMessageBox::Information, "test",
//               tr("Key is ") + s, QMessageBox::Ok);
//   msg.exec();
   model_ref_.setHeaderData(0, Qt::Horizontal, QObject::tr("Identifier"));
   model_ref_.setHeaderData(1, Qt::Horizontal, QObject::tr("Type"));
   QTableView *view = new QTableView();
   view->setModel(&model_ref_);

   // Boutons
   QPushButton *btn_add = new QPushButton(tr("Add..."));
   connect(btn_add, SIGNAL(clicked(bool)), this, SLOT(AddEntry()));
   QPushButton *btn_remove = new QPushButton(tr("Delete"));
   connect(btn_remove, SIGNAL(clicked(bool)), this, SLOT(RemoveEntry()));
   QPushButton *btn_save = new QPushButton(tr("Save"));
   connect(btn_save, SIGNAL(clicked(bool)), this, SLOT(Save()));

   // Groupe des boutons
   QGroupBox *gb = new QGroupBox();
   QVBoxLayout *vbox = new QVBoxLayout;
   vbox->addWidget(btn_add);
   vbox->addWidget(btn_remove);
   vbox->addWidget(btn_save);
   vbox->addStretch(1);
   gb->setLayout(vbox);

   QHBoxLayout *layout = new QHBoxLayout(this);
   layout->addWidget(view);
   layout->addWidget(gb);

   setWindowModality(Qt::ApplicationModal);
   // widget supprimé lorsqu'il est fermé
   setAttribute(Qt::WA_DeleteOnClose, true);
}


TableViewWindow::~TableViewWindow(){

}


// =============================================================================
//
//  SLOTS ET SIGNAUX
//
// =============================================================================

void TableViewWindow::AddEntry() {
//   QMessageBox msg(QMessageBox::Information, "test",
//               tr("Key is ") + model_ref_.primaryKey().name(), QMessageBox::Ok);
//   msg.exec();
   TableEntryDialog dlg(tr("New entry"), this);
   if (dlg.exec() == QDialog::Accepted) {
      QSqlRecord rec = model_ref_.record();
      rec.setValue(1, dlg.Entry());
      model_ref_.insertRecord(-1, rec);
   }
}

void TableViewWindow::RemoveEntry(){

}

void TableViewWindow::Save(){

}
