// =============================================================================
//  BibSci - Bibliothèque scientifique
// -----------------------------------------------------------------------------
//  src/main.cpp
// -----------------------------------------------------------------------------
//  Copyright (C) 2024 - Graouilly
//
//  This file is part of BibSci.
//
//  BibSci is free software: you can redistribute it and/or modify it under the
//  terms of the GNU General Public License as published by the Free Software
//  Foundation, version 3 of the License.
//  BibSci is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
//  details.
// 
//  You should have received a copy of the GNU General Public License along with
//  BibSci. If not, see <https://www.gnu.org/licenses/>
// =============================================================================

// Inclusions C

// Inclusions C++

// Inclusions Qt
#include <QApplication>     // QApplication
#include <QSqlDatabase>     // QsqlDatabase

// Inclusions internes
#include "main_window.hpp"  // MainWindow

int main(int argc, char *argv[])
{ 
   QApplication app(argc, argv);
   // la base de données générale représentant la bibilothèque est créée ici,
   // afin d'être active tout au long de l'application.
   // Elle n'est cependant peuplée que par MainWindow et ses classes affiliées.
   QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
   MainWindow w;
   w.show();
   return app.exec();
}
